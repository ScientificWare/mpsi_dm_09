/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpsi_dm_09;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author ScientificWare
 */
public class MPSI_DM_09 extends Application {

    @Override
    public void start(Stage primaryStage) {
        VBox vb = new VBox();
        Label lb1 = new Label("Entrer le nombre de terme souhaité");
        TextField tf = new TextField();
        TextArea tf2 = new TextArea();

        Button btn = new Button();
        btn.setText("Calculer");
        Label lb2 = new Label("Liste des valeurs de la suite U\u2099");
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                int in = Integer.valueOf(tf.getText());
                double Un = 0.0f;
                for (int i = 0; i <= in; i++) {
                    Un = 1.0f / 2 * Math.acos(Un);
                    tf2.insertText(tf2.getText().length(), "U" + i + "=" + Un + "\n");
                }
            }
        });

        StackPane root = new StackPane();
        vb.getChildren().addAll(lb1, tf, btn, lb2, tf2);
        root.getChildren().add(vb);

        Scene scene = new Scene(root, 300, 250);

        primaryStage.setTitle("MPSI DM 9 Mathématiques");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
